import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(Vuex)
Vue.use(axios,VueAxios)

export default () => {
  return new Vuex.Store({
    state: {
      count: 0,
      currentx: 1,
      totalpages: 1,
      isLoad: false,
      removeContent: '',
      units: [],
      systems: [],
      levels: [],
      lessonOptions: [],
      subjects: [],
      classes: [],
      questions: [],
      ParagraphQuestions: [],
      form: {
        system: "",
        level: "",
        class: "",
        semester: "",
        subject: "",
        unit: "",
        lesson: ""
      },
      "normal-ques-image": "",
      "paragraph-answer-data-question": "",
      "complete-answer-data":[''],
      "complete-head":"",

      "choose-answer-data":[''],
      "choose-head":"",


      "truefalse-answer-data":'',
      "truefalse-head":"",


      "paragraph-answer-data":"",
      "paragraph-head":"",
      "paragraph-url":"",

      normalQues: {},
      paraQues: {},

      bgColorQues: 'blue',
      quesContent: "اضافة الي اسئلة الامتحان",
      questionsExam: [],
      main: true,

    },
    getters:{
      "complete-answer-data-getters": state => {
        return state["complete-answer-data"] = state.normalQues.modelAnswer
      },
      "complete-head-getters": state => {
        return state["complete-head"] = state.normalQues.head
      },


      "choose-answer-data-getters": state => {
        return state["choose-answer-data"]  = state.normalQues.modelAnswer
      },
      "choose-head-getters": state => {
        return state["choose-head"] = state.normalQues.head
      },


      "truefalse-answer-data-getters": state => {
        return state["truefalse-answer-data"] = state.normalQues.modelAnswer
      },
      "truefalse-head-getters": state => {
        return state["truefalse-head"] = state.normalQues.head
      },


      "paragraph-answer-data-getters": state => {
        return state["paragraph-answer-data"]
      },
      "paragraph-head-getters": state => {
        return state["paragraph-head"]
      },


      oldVal: state => {
        return state.count
      },
      changeSystems: state => {
        return state.form.system
      },
      changeLevel: state => {
        return state.form.level
      },
      changeClass: state => {
        return state.form.class
      },
      changeSubject: state => {
        return state.form.subject
      },
      changeUnits: state => {
        return state.form.unit
      },
      changeLesson: state => {
        return state.form.lesson
      },
      questionBtnContent: state => {
        return state.quesContent
      }
    },
    
    mutations: {
      "complete-answer-data-mutation": (state, payload) => {
        return state["complete-answer-data"].push(payload)
      },
      "complete-head-mutation": (state, payload) => {
        return state["complete-head"] = payload
      },
      "choose-answer-data-mutation": (state, payload) => {
        return state["choose-answer-data"] = payload
      },
      "choose-head-mutation": (state, payload) => {
        return state["choose-head"] = payload
      },


      "truefalse-answer-data-mutation": (state, payload) => {
        return state["truefalse-answer-data"] = payload 
      },
      "truefalse-head-mutation": (state, payload) => {
        return state["truefalse-head"] = payload
      },
      

      "paragraph-answer-data-mutation": (state, payload) => {
        return state["paragraph-answer-data"] = payload 
      },
      "paragraph-head-mutation": (state, payload) => {
        return state["paragraph-head"] = payload
      },
      main: (state, payload) => {
        return state.main = payload;
      },
      checkifQuestionExist: function(state, payload){
        var x = 0
        state.questionsExam.forEach(ele => {
          if(ele.id == payload){
            x = 1
          }else{
            x = 0
          }
        })
        return x;
      },
      x: (state,payload) => {
       return state.count = payload
      },
      systemChangeM: (state, payload) => {
        state.form.system = payload
      },
      levelBind: (state, payload) => {
        state.form.level = payload
      },
      classBind: (state, payload) => {
        state.form.class = payload
      },
      subjectBind: (state, payload) => {
        state.form.subject = payload
      },
      unitsBind: (state, payload) => {
        state.form.unit = payload
      },
      lessonBind: (state, payload) => {
        state.form.lesson = payload
      },

     
      
    },
    actions:{
      
  
      // getDifficulty: function({state}) {
      //   state.isLoad = true;
      //   axios
      //     .get(
      //       `https://basthalk-be.herokuapp.com/api/v1/subjects/${state.form.subject.id}/questions?page=${state.currentx}&difficultyLevel=${state.form.difficulty.value}`
      //     )
      //     .then(res => {
      //       state.totalpages = res.data.totalPages;
      //       console.log("this is questions ", res);
      //       (state.ParagraphQuestions = []), (state.questions = []);
      //       res.data.docs.filter(question => {
      //         if (question.type == "paragraph" || question.type == "group") {
      //           state.ParagraphQuestions.push(question);
      //         } else {
      //           state.questions.push(question);
      //         }
      //       });
      //     })
      //     .finally(() => {
      //       state.isLoad = false;
      //     });
      // },
  
      
  
      
     
      
      // classInput({state}) {
      //   console.log(state.classes);
      // },

      
    },
    
  })
}


/*
 addNormalQuestion: function(obj,index,e){


    e.target.classList.toggle('Toggled')
    // if(e.target.classList.contains('Toggled')){
    //   e.target.classList.replace('btn-primary','btn-danger')
    //   e.target.textContent = "حذف من أسئلة الامتحان"
      
    // }else{
    //   e.target.textContent = "أضافة الي أسئلة الأمتحان"
      
    //   e.target.classList.replace('btn-danger','btn-primary')
    // }

      if(this.checkifQuestionExist(obj.id) !== 0){
        console.log("true")
        obj.isExamQues = false
        e.target.style.backgroundColor = this.bgColorQues
         e.target.textContent = "اضافة الي اسئلة الامتحان"
         this.bgColorQues = '#058ac6'
          e.target.style.backgroundColor = this.bgColorQues
        this.questionsExam.splice(index,1)
        return ;
      }else{
        obj.isExamQues = true
        console.log(obj.id)
        //  if(obj.isExamQues == true){
          console.log("The Obj is true")
          
         e.target.textContent = "حذف من قائمة الأسئلة"
           this.bgColorQues = '#e21d1d'
          e.target.style.backgroundColor = this.bgColorQues
        this.questionsExam.push(obj)
        // }
         
      }
    
  },
  addGroupQuestion: function(obj,index,e){

     e.target.classList.toggle('Toggled')
    if(e.target.classList.contains('Toggled')){
      e.target.classList.replace('btn-primary','btn-danger')
      e.target.textContent = "حذف من أسئلة الامتحان"
    }else{
      e.target.textContent = "أضافة الي أسئلة الأمتحان"
      e.target.classList.replace('btn-danger','btn-primary')
    }


     if(this.checkifQuestionExist(obj.id) !== undefined ){
        console.log("true")
        this.questionsExam.splice(index,1)
        return ;
      }else{
        this.questionsExam.push(obj)
      }
  },
 

*/